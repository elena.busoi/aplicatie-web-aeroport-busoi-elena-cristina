<?php
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    require './vendor/phpmailer/phpmailer/src/Exception.php';
    require './vendor/phpmailer/phpmailer/src/PHPMailer.php';
    require './vendor/phpmailer/phpmailer/src/SMTP.php';

   session_start();

   if (isset($_SESSION['userID']) &&
       isset($_SESSION['userEmail'])


   ) {




   } else {
       header('location: index.php');
   }


// if(isset($_POST['submit'])){
//     $phpmailer = new PHPMailer();
//     $phpmailer->isSMTP();
//     $phpmailer->Host = 'sandbox.smtp.mailtrap.io';
//     $phpmailer->SMTPAuth = true;
//     $phpmailer->Port = 2525;
//     $phpmailer->Username = '91d70720940fb8';
//     $phpmailer->Password = 'a3a129dfcae253';
//     $mail->isHTML(true);

//     $phpmailer->setFrom('info@mailtrap.io', 'Mailtrap');
//     $phpmailer->addReplyTo('info@mailtrap.io', 'Mailtrap');
//     $phpmailer->addAddress('recipient1@mailtrap.io', 'Tim');
//     $phpmailer->addCC('cc1@example.com', 'Elena');
//     $phpmailer->addBCC('bcc1@example.com', 'Alex');
//     $mailContent = "
//         <h1>Send HTML Email using SMTP in PHP</h1>
//         <p>This is a test email I’m sending using SMTP mail server with PHPMailer.</p>
//     ";
//     $phpmailer->Body = $mailContent;
    
//     if($phpmailer->send()){
//         echo "<div id='success_message'>Mesajul tau a fost trimis cu succes.</div>";
//     } else{
//         echo "<div id='error_message'>Mesajul nu a fost trimis.</div>";
//     }
// }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<link rel="stylesheet" type="text/css" href="profil.css">

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
</head>
<body>
<body>
<nav class="navbar navbar-dark" >
    <button type="button" class="btn btn-outline-light button_menu" id="info">Informații</button>
        <button type="button" class="btn btn-outline-light button_menu" id="rezerva">Rezervă bilete</button>
        <button type="button" class="btn btn-outline-light button_menu " id="parcare">Parcare</button>
        <button type="button" class="btn btn-outline-light button_menu" id="contact">Contact</button>


        <div class="nav-link dropdown-toggle b1" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class='fas fa-user-tie' style='font-size:30px'></i>
        <?php echo  $_SESSION['userFname']; echo " ";  echo  $_SESSION['userLname']; ?>
       </div>
       <div class="dropdown-menu  " aria-labelledby="navbarDropdown" style="margin-left:85%;">
          <a class="dropdown-item " href="profil.php">
             <button type="button" class="btn btn-link logout" id="log" >Profil</button>
          </a>
          <a class="dropdown-item " href="#">
             <button type="button" class="btn btn-link logout" id="logout" >Deconectare</button>
          </a>
        </div>
       <!-- <div>
       <button type="button" class="btn btn-link logout" id="logout" >Deconectare</button>
       </div> -->



    </nav>
<div id="form-main" >
                <div id="form-div" class="contact-form">
                    <div class="montform" id="reused_form" >

                        <p class="name">
                            <input name="name" type="text" class="feedback-input" required placeholder="Nume" id="name" />
                        </p>
                        <p class="email">
                            <input name="email" type="email" required class="feedback-input" id="email" placeholder="Email" />
                        </p>
                        <p class="text">
                            <textarea name="message" class="feedback-input" id="comment" placeholder="Mesaj"></textarea>
                        </p>
                        <div class="submit">
                            <button  class="button-blue" id="trimite" style="display: flex; align-items: center; justify-content: space-around;">
                                <span>TRIMITE</span>
                                <div class="spinner-border spinner-border-sm" id="loadingMail" role="status" style="display: none;">
                                    <span class="sr-only">Loading...</span>
                                </div>
                            </button>
                            <div class="ease"></div>
                        </div>
                    </div>
                    <div id="error_message" style="width:100%; height:100%; display:none; color: white;">
                        <h4>
                            Error
                        </h4>
                        Mesajul nu a fost trimis.
                    </div>
                    <div id="success_message" style="width:100%; height:100%; display:none; color: white;"> 
                        <h4>
                            Success
                        </h4>
                        Mesajul tău a fost trimis cu succes.
                    </div>
                </div>
            </div>

     <script src="logout.js"></script>
    <script src="profil_menu.js"></script>
    <script src="suport.js"></script>

</body>
</html>