<?php

    session_start();

    if (isset($_SESSION['userID']) &&
        isset($_SESSION['userEmail'])


    ) {




    } else {
        header('location: index.php');
    }

?>


<!DOCTYPE html>
<html>
<head>
<title>Rezervă bilete</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> -->
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" /> -->
<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script> -->

<link rel="stylesheet" type="text/css" href="rezerva_bilete.css">

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script> -->

<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

</head>

<body>
    <nav class="navbar navbar-dark" >
    <button type="button" class="btn btn-outline-light button_menu" id="info">Informații</button>
        <button type="button" class="btn btn-outline-light button_menu" id="rezerva">Rezervă bilete</button>
        <button type="button" class="btn btn-outline-light button_menu " id="parcare">Parcare</button>
        <button type="button" class="btn btn-outline-light button_menu" id="contact">Contact</button>


        <div class="nav-link dropdown-toggle b1" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class='fas fa-user-tie' style='font-size:30px'></i>
        <?php echo  $_SESSION['userFname']; echo " ";  echo  $_SESSION['userLname']; ?>
       </div>
       <div class="dropdown-menu  " aria-labelledby="navbarDropdown" style="margin-left:85%;">
          <a class="dropdown-item " href="profil.php">
             <button type="button" class="btn btn-link logout" id="log" >Profil</button>
          </a>
          <a class="dropdown-item " href="#">
             <button type="button" class="btn btn-link logout" id="logout" >Deconectare</button>
          </a>
        </div>
       <!-- <div>
       <button type="button" class="btn btn-link logout" id="logout" >Deconectare</button>
       </div> -->



    </nav>
    <div class="f0">
    <div class="f1">
        <p class="p1"  style="margin-left:20px;">Zboruri</p>
        <form>
        <!-- <div class="form-check form-check-inline" style="margin-left:20px;">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="dus">
            <label class="form-check-label" for="inlineRadio1" style="color:white; font-size: 17px;">Doar dus</label>
        </div> -->
        <div class="form-check form-check-inline" style="margin-left:20px;">
            <input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="intors" value="intors">
            <label class="form-check-label" for="inlineRadio2"style="color:white; font-size: 17px;">Dus-întors</label>
        </div>

        <div style="margin-left:20px" >
            <br>
            <div class="row">
    <div class="col-3">
        <select class="input" id="oras_plecare" style="height:35px;">
            <option value="" disabled selected>Selectați orașul de plecare</option>
            <option value="Bucuresti">București</option>
            <option value="Timisoara">Timișoara</option>
            <option value="Paris">Paris</option>
            <option value="Londra">Londra</option>
            <option value="Cluj">Cluj</option>
            <option value="Roma">Roma</option>
            <option value="Craiova">Craiova</option>
        </select>
    </div>
    <div class="col-3">
        <select class="input" id="destinatie" style="height:35px;">
            <option value="" disabled selected>Selectați destinația</option>
            <option value="Bucuresti">București</option>
            <option value="Timisoara">Timișoara</option>
            <option value="Paris">Paris</option>
            <option value="Londra">Londra</option>
            <option value="Cluj">Cluj</option>
            <option value="Roma">Roma</option>
            <option value="Craiova">Craiova</option>
        </select>
    </div>
    <div class="col-3" id="date1">
        <input type="date" id="datapicker1" value="" placeholder="Dată Plecare" style="width: 150px; height: 35px;" />
    </div>
    <div class="col-3" id="date2" style="display: none;">
        <input type="date" id="datapicker2" value="" placeholder="Dată Retur" style="width: 150px; height: 35px;" />
    </div>
</div>
            <br>
            <span style="color: white;">Număr pasageri</span>
            <br>
            <select class="selectpicker" style="width:50px; height:30px" id="locuri" >
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
            </select>

            <button type="button" class="btn btn-success" style="width:150px; margin-left:20%;" id="cauta">Caută curse</button>





        </div>



        </form>

    </div>
</div>

<div class="f2" style=" background-color: #f9c9b5;">






</div>





    <script src="rezerva_bilete.js"></script>
    <script src="logout.js"></script>
    <script src="profil_menu.js"></script>


</body>
</html>
