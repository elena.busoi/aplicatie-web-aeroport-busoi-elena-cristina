<?php
    session_start();

    if (isset($_SESSION['userID']) &&
        isset($_SESSION['userEmail'])


    ) {




    } else {
        header('location: index.php');
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Parcare</title>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./CSS/bootstrap.min.css">
    <link rel="stylesheet" href="./CSS/styleP.css">
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> -->
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" /> -->


<link rel="stylesheet" type="text/css" href="rezerva_bilete.css">

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script> -->

<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
</head>
<body style="background-color:lightblue;">
<nav class="navbar navbar-dark" >
    <button type="button" class="btn btn-outline-light button_menu" id="info">Informații</button>
        <button type="button" class="btn btn-outline-light button_menu" id="rezerva">Rezervă bilete</button>
        <button type="button" class="btn btn-outline-light button_menu " id="parcare">Parcare</button>
        <button type="button" class="btn btn-outline-light button_menu" id="contact">Contact</button>


        <div class="nav-link dropdown-toggle b1" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class='fas fa-user-tie' style='font-size:30px'></i>
        <?php echo  $_SESSION['userFname']; echo " ";  echo  $_SESSION['userLname']; ?>
       </div>
       <div class="dropdown-menu  " aria-labelledby="navbarDropdown" style="margin-left:85%;">
          <a class="dropdown-item " href="profil.php">
             <button type="button" class="btn btn-link logout" id="log" >Profil</button>
          </a>
          <a class="dropdown-item " href="#">
             <button type="button" class="btn btn-link logout" id="logout" >Deconectare</button>
          </a>
        </div>



    </nav>




    <div class="form-container mt-5">
        <form class="w-50 mx-auto" id="entryForm">
            <h5 class="text-center">Rezervă un loc de parcare</h5>
            <div class="form-group">
                <label for="owner">Proprietar:</label>
                <input type="text" class="form-control rounded-0 shadow-sm" id="owner" placeholder="Proprietar">
            </div>
            <div class="form-group">
                <label for="car">Marcă Autoturism:</label>
                <input type="text" class="form-control rounded-0 shadow-sm" id="car" placeholder="Marcă Autoturism">
            </div>
            <div class="form-group">
                <label for="licensePlate">Număr de Înmatriculare:</label>
                <input type="text" class="form-control rounded-0 shadow-sm" id="licensePlate" placeholder="trebuie sa fie de forma B100BAR">
            </div>
            <div class="row">
                <div class="col-6">
                    <label for="entryDate">Dată Intrare:</label>
                    <input type="date" class="form-control rounded-0 shadow-sm" id="entryDate">
                </div>
                <div class="col-6">
                    <label for="exitDate">Dată Ieșire:</label>
                    <input type="date" class="form-control rounded-0 shadow-sm" id="exitDate">
                </div>
            </div>
            <div class="text-center mx-auto" id="dateDifference" style="display: none;"></div>
        <div class="text-center mx-auto" id="priceElement" style="display: none;">
        Pret:
        <span  id="priceText">   </span> Lei
    </div>
            <button type="submit" class="btn mx-auto d-block mt-5 rounded-0 shadow" id="btnOne">Adaugă mașină</button>
        </form>
    </div>
    </div>

    <?php
?>
    <div class="table-container mt-5 mb-5 w-75 mx-auto" >
            <h5 class="text-center mb-3">Rezervările tale</h5>
            <input type="text" class="w-100 mb-3" id="searchInput" placeholder="Search...">
            <table class="table table-striped shadow " id="parkingTable">
                    <thead class="text-white" id="tableHead">
                      <tr>
                        <th scope="col">Propietar</th>
                        <th scope="col">Mașină</th>
                        <th scope="col">Număr Înmatriculare</th>
                        <th scope="col">Dată Intrare</th>
                        <th scope="col">Dată Ieșire</th>  
                        <th scope="col">Preț</th>                                   
                        <th scope="col">Acțiune</th>
                      </tr>
                    </thead>
                    <tbody id="tableBody">
                     
                    </tbody>
                  </table>
    </div>


    


  




    </div>

    <script src="logout.js"></script>
    <script src="rezerva_bilete.js"></script>
    <script src="profil_menu.js"></script>
    <script src="./JS/bootstrap.min.js"></script>
    <script src="./JS/core.js"></script>
    <script>
function calculateDateDifference() {
    var entryDate = document.getElementById("entryDate").value;
    var exitDate = document.getElementById("exitDate").value;

    if (exitDate !== "") {
        var date1 = new Date(entryDate);
        var date2 = new Date(exitDate);

        var differenceInTime = date2.getTime() - date1.getTime();
        var differenceInDays = Math.ceil(differenceInTime / (1000 * 3600 * 24));

        var dateDifferenceElement = document.getElementById("dateDifference");
        dateDifferenceElement.textContent = "Durata Parcare: " + differenceInDays + " zile";
        dateDifferenceElement.style.display = "block";

        var price = differenceInDays * 10; // Calculating the price (assuming 10 lei per day)

        var priceElement = document.getElementById("priceElement");
        var priceText = document.getElementById("priceText");
        priceText.textContent =  price;
        priceElement.style.display = "block";


    } else {
        var dateDifferenceElement = document.getElementById("dateDifference");
        dateDifferenceElement.style.display = "none";

        var priceElement = document.getElementById("price");
        priceElement.style.display = "none";
    }
}


    document.getElementById("entryForm").addEventListener("submit", function (event) {
        event.preventDefault(); // Prevent form submission
        calculateDateDifference();
    });

    document.getElementById("entryDate").addEventListener("input", calculateDateDifference);
    document.getElementById("exitDate").addEventListener("input", calculateDateDifference);
</script>

    </body>
    </html>
