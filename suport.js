$('#trimite').click(function(e){
    e.stopPropagation();

    $('#loadingMail').show();

    $.ajax({
        url: 'suport.php',
        method: 'POST',
        data: {
            mail: $('#email').val(),
            mesaj: $('#comment').val(),
            nume: $('#name').val()
        },

        success: function(response) {
            var data = JSON.parse(response);
            if(data.msg == 'succes'){
                $('#error_message').hide();
                $('#success_message').show();
            }
            else {
                $('#error_message').show();
                $('#success_message').hide();
            }

            $('#email').val('');
            $('#comment').val('');
            $('#name').val('');
            $('#loadingMail').hide();
        }
    });
});