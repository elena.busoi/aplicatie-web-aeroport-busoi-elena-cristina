// $(function () {
//     $("#datapicker1").datepicker({
//           autoclose: true,
//           todayHighlight: true
//     }).datepicker('update', new Date());
// });

// $('#datapicker1').datepicker({
//     dateFormat: 'dd-mm-yyyy',
//     uiLibrary: 'bootstrap4',
//     width:"200px"
// });

// $('#datapicker2').datepicker({
//     dateFormat: 'dd-mm-yyyy',
//     uiLibrary: 'bootstrap4',
//     width:"200px"
// });

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [month, day, year].join('/');
}

$("#date1").fadeIn();
$('#intors').click(function(){
    if($('input#intors').prop("checked")){
    $("#date2").fadeIn();}
    else
    $("#date2").fadeOut();
});



$(document).ready(()=>{
    $('#cauta').click(function(){

        $.ajax({
            url: 'curse.php',
            method: 'POST',
            data:{
               intors: $('input#intors').prop("checked"),
               orasP: $('#oras_plecare').val(),
               orasD: $('#destinatie').val(),
               dataP: formatDate($('#datapicker1').val()),
               dataR: formatDate($('#datapicker2').val()),
               locuri: $('#locuri').val(),
               pret: parseFloat($('.pret').text()) // Adăugați prețul aici

            },

            success: function(response){
                var data=JSON.parse(response);

                var html = '<table class="table table-striped">';
                html += '<thead><tr>';
                html += '<th>Oras plecare</th><th>Oras destinatie</th><th>Data</th><th>Numar locuri</th><th>Durata</th><th>Companie</th><th>Detalii</th><th>Rezerva bilet</th>';
                html += '</tr></thead><tbody>';
                
                $.each(data, function(index, value) {
                  var pretInitial = parseFloat(value["pret"]); 
                  html += '<tr>';
                  html += '<td>' + value["orasP"] + '</td>';
                  html += '<td>' + value["orasD"] + '</td>';
                  html += '<td>' + value["dataP"] + '</td>';
                  html += '<td>' + value["locuri"] + '</td>';
                  html += '<td>' + value["durata"] + '</td>';
                  html += '<td>' + value["companie"] + '</td>';
                  

                  html += '<td><button class="btn btn-primary detalii-btn" data-toggle="collapse" data-target="#detalii-' + value["id"] + '">Detalii</button></td>';
                  html += '<td><button class="btn btn-success rezerva" id="rezerva" style="width:150px; margin-left:20%;" data-id="' + value["id"] + '" data-total="' + value["locuri"] + '">Rezerva bilete</button></td>';
                  html += '</tr>';
                  html += '<tr class="collapse" id="detalii-' + value["id"] + '">';
                  html += '<td colspan="8">';
                  html += '<strong>Pret:</strong> <span class="pret">' + value["pret"] + '</span> Lei <br>';
                  html += '<strong>Ora Plecare:</strong> ' + value["ora"] + '<br>';
                  html += '<strong>Clasa:</strong> <select class="clasa-select">';
                  html += '<option value="Economic">Economic</option>';
                  html += '<option value="Business">Business</option>';
                  html += '</select><br>';
                  html += '</td>';
                  html += '</tr>';
                });
                
                html += '</tbody></table> <div style="position:absolute;bottom:20px;right:20px; font-size:40px; cursor:pointer;"><i id="go_up" class="far fa-arrow-alt-circle-up"></i></div>';
                
                
                $(document).ready(function() {
                  $('#tabel-curse').html(html);
                
                  
                  $(document).on('click', '.detalii-btn', function() {
                    var target = $(this).data('target');
                    $(target).collapse('toggle'); 
                
                    var buttonText = $(this).text();
                    if (buttonText === 'Detalii') {
                      $(this).text('Ascunde');
                    } else {
                      $(this).text('Detalii');
                    }
                  });
                
                 
                  $(document).on('change', '.clasa-select', function() {
                    var selectedOption = $(this).val();
                    var pretElement = $(this).closest('td').find('.pret');
                    var pretInitial = parseFloat(pretElement.text());
                
                    if (selectedOption === 'Business') {
                     
                      pretElement.text(pretInitial.toFixed(2) * 1.75);
                    } else {
                        pretElement.text(pretInitial.toFixed(2) / 1.75);
                      }   
                  });
                });
                
                

                
                
                
                
                    $('.f2').html(html).show();
                    $([document.documentElement, document.body]).animate({
                        scrollTop: $(".f2").offset().top
                    }, 500);
                    $('#go_up').click(function(){
                        $([document.documentElement, document.body]).animate({
                        scrollTop: $(".f0").offset().top
                    }, 500);
                     });

                    $(".rezerva").click(function(e){
                        e.stopPropagation();
                        var id=$(this).data().id;                        
                        var total=$(this).data().total;                        
                        $.ajax({
                            url: 'rezerva_curse.php',
                            method: 'POST',
                            data:{
                               id_cursa: id,
                               locuri: $('#locuri').val(),
                               total: total,
                               pret: parseFloat($('.pret').text()) // Adăugați prețul aici


                            },

                            success: function(response){
                               alert(response);
                            

                            }

                        });





                    })


            }


        });




    })




})

$(document).ready(function() {
  // Salvați opțiunile inițiale ale meniului dropdown al destinației
  var initialDestinations = $('#destinatie option').clone();

  $('#oras_plecare').change(function() {
    var selectedOption = $(this).val();

    // Restabiliți opțiunile inițiale ale meniului dropdown al destinației
    $('#destinatie').html(initialDestinations);

    // Eliminați opțiunea selectată în meniul dropdown al plecării din meniul dropdown al destinației
    $('#destinatie option[value="' + selectedOption + '"]').remove();
  });
});
