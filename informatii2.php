<?php

    session_start();

    if (isset($_SESSION['userID']) &&
        isset($_SESSION['userEmail'])


    ) {




    } else {
        header('location: index.php');
    }

?>


<!DOCTYPE html>
<html>
<head>
<title>Informatii</title>


<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> -->
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" /> -->


<link rel="stylesheet" type="text/css" href="rezerva_bilete.css">
<link rel="stylesheet" type="text/css" href="informatii.css">


<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script> -->

<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

</head>

<body class="f4">
    <nav class="navbar navbar-dark" >
    <button type="button" class="btn btn-outline-light button_menu" id="acasa">Acasă</button>
        <button type="button" class="btn btn-outline-light button_menu" id="rezerva">Rezervă bilete</button>
        <button type="button" class="btn btn-outline-light button_menu " id="parcare">Parcare</button>
        <button type="button" class="btn btn-outline-light button_menu" id="contact">Contact</button>


        <div class="nav-link dropdown-toggle b1" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class='fas fa-user-tie' style='font-size:30px'></i>
        <?php echo  $_SESSION['userFname']; echo " ";  echo  $_SESSION['userLname']; ?>
       </div>
       <div class="dropdown-menu  " aria-labelledby="navbarDropdown" style="margin-left:85%;">
          <a class="dropdown-item " href="profil.php">
             <button type="button" class="btn btn-link logout" id="log" >Profil</button>
          </a>
          <a class="dropdown-item " href="#">
             <button type="button" class="btn btn-link logout" id="logout" >Deconectare</button>
          </a>
        </div>
       <!-- <div>
       <button type="button" class="btn btn-link logout" id="logout" >Deconectare</button>
       </div> -->

    </nav>


    <div class="container">
  <nav>
    <ul>
      <li><a href="#section1">Bagaje</a></li>
      <li><a href="#section2">Servicii la bord</a></li>
      <li><a href="#section3">Asistenta</a></li>
    </ul>
  </nav>
  <div class="content">
    <section id="section1">
      <h2>Element 1</h2>
      <p>Acesta este textul pentru Element 1.</p>
    </section>
    <section id="section2">
      <h2>Element 2</h2>
      <p>Acesta este textul pentru Element 2.</p>
    </section>
    <section id="section3">
      <h2>Element 3</h2>
      <p>Acesta este textul pentru Element 3.</p>
    </section>
  </div>
</div>

    <script src="rezerva_bilete.js"></script>
    <script src="logout.js"></script>
    <script src="profil_menu.js"></script>
</body>   