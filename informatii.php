<?php

    session_start();

    if (isset($_SESSION['userID']) &&
        isset($_SESSION['userEmail'])


    ) {




    } else {
        header('location: index.php');
    }

?>


<!DOCTYPE html>
<html>
<head>
<title>Informatii</title>


<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> -->
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" /> -->


<link rel="stylesheet" type="text/css" href="rezerva_bilete.css">
<link rel="stylesheet" type="text/css" href="informatii.css">


<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script> -->

<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

</head>

<body >

<nav class="navbar navbar-dark" >
    <button type="button" class="btn btn-outline-light button_menu" id="info">Informații</button>
        <button type="button" class="btn btn-outline-light button_menu" id="rezerva">Rezervă bilete</button>
        <button type="button" class="btn btn-outline-light button_menu " id="parcare">Parcare</button>
        <button type="button" class="btn btn-outline-light button_menu" id="contact">Contact</button>


        <div class="nav-link dropdown-toggle b1" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class='fas fa-user-tie' style='font-size:30px'></i>
        <?php echo  $_SESSION['userFname']; echo " ";  echo  $_SESSION['userLname']; ?>
       </div>
       <div class="dropdown-menu  " aria-labelledby="navbarDropdown" style="margin-left:85%;">
          <a class="dropdown-item " href="profil.php">
             <button type="button" class="btn btn-link logout" id="log" >Profil</button>
          </a>
          <a class="dropdown-item " href="#">
             <button type="button" class="btn btn-link logout" id="logout" >Deconectare</button>
          </a>
        </div>
       <!-- <div>
       <button type="button" class="btn btn-link logout" id="logout" >Deconectare</button>
       </div> -->



    </nav>

    <div class="container">
  
  <h1>Informații</h1>

  <div class="tabs">
    <div class="tabby-tab">
      <input type="radio" id="tab-1" name="tabby-tabs" checked>
      <label for="tab-1">Bagaj</label>
      <div class="tabby-content">
        <img src="bagaje.jpg"/>
        <p>Orice bagaj pe care îl luaţi cu dumneavoastră în cabina de pasageri reprezintă bagajul de mână.Ca pasager  aveţi dreptul de a transporta în cabină un bagaj de mână ce nu trebuie să depăşească următoarele dimensiuni: 55x 40x 20cm (lungime x lăţime x înălţime), în cazul în care călătoriţi la bordul unei aeronave de tip Boeing sau Airbus.În cazul în care călătoriţi la bordul unei aeronave de tip ATR 42 / ATR 72, bagajul transportat în cabină nu trebuie să depăşească dimensiunile de: 35x 30 x 20cm (lungime x lăţime x înălţime).De asemenea, puteţi transporta gratuit şi un obiect personal (ex. poşeta sau servietă). Greutatea totală (bagajul de cabină şi obiectul personal) nu trebuie să depăşească 8 Kg.<br>
        Pentru a fi informaţi, vă recomandăm să consultaţi website-urile de specialitate ale companiilor respective.<br>
        Află aici mai multe detalii despre bunurile interzise a fi utilizate la bordul aeronavei.
    Fumatul, inclusiv utilizarea materialelor electronice pentru simularea fumatului (țigări electronice, pipe electronice, trabucuri electronice, etc.) sunt interzise la bordul aeronavei.
    Materialele electronice pentru simularea fumatului sunt interzise în bagajul de cală. Acestea pot fi acceptate la bordul aeronavei, numai în bagajul de mână, cu condiția ca:
   Acestea să rămână depozitate pe toata durata zborului și
Neutilizate în bagajul dumneavoastră de mână.
Reîncărcarea țigărilor electronice și a bateriilor acestora nu este permisă la bordul aeronavei.<br>
Dispozitive electronice<br>
Dispozitivele electronice pot fi utilizate în timpul zborului numai în modul avion.<br>
În cazul în care dispozitivele dumneavoastră electronice nu au această funcție, vă rugăm să le opriți pe toată durata zborului.
Reîncărcarea dispozitivelor electronice și a bateriilor acestora, inclusiv utilizarea bateriilor externe de orice tip, nu este permisă la bordul aeronavei.</p>
      </div>
    </div>

    <div class="tabby-tab">
      <input type="radio" id="tab-2" name="tabby-tabs">
      <label for="tab-2">Documente necesare </label>
      <div class="tabby-content">
        <img src="documente.png"/>
        <p>Înainte de a pleca într-o călătorie verificaţi dacă aveţi toate actele necesare la dumneavoastră:<br>
paşaport sau carte de identitate pentru ţările care permit accesul doar în baza cărţii de identitate;<br>
viză de acces sau permis de rezidenta pentru ţările pentru care deţinerea unui astfel de document este obligatorie.<br>
Pentru mai multe informaţii, verificaţi condiţiile de acces în statul de tranzit şi / sau de destinaţie, accesând <a href="http://www.skyteam.com/en/flights-and-destinations/visa-and-health/" target="_blank">Click Aici</a> 
Compania  nu îşi asumă nici o responsabilitate pentru pasagerii cu documente de transport necorespunzătoare.
Un pasager nu poate fi acceptat la transport fără documentele şi formalităţile necesare în ţara de intrare, tranzit şi ieşire (paşapoarte, vize, certificate de sănătate etc.)
 Anumite state au impus condiţii de călătorie din cauza pandemiei - Covid-19. Vă rugăm să verificaţi aici ce alte documente sunt necesare la îmbarcare, in funcţie de destinaţie.
Compania  îşi rezervă dreptul de a refuza la îmbarcare orice pasager care nu se află în posesia unui document care să facă dovada identităţii pasagerului în cauză.</p>
      </div>
    </div>

    <div class="tabby-tab">
      <input type="radio" id="tab-3" name="tabby-tabs">
      <label for="tab-3">Arme și muniții</label>
      <div class="tabby-content">
          <img src="arma.jpg"/>
          <p>Condiţii de transport pentru arme şi muniţii pe zborurile operate cu aeronave proprii.<br>
     Urmare a cerinţelor Uniunii Europene în domeniul securităţii aviaţiei civile, transportul armelor şi muniţiilor nu este acceptat în cabina de pasageri a aeronavelor . Acestea pot fi transportate numai ca bagaje de cală sau cargo, cu respectarea următoarelor condiţii:<br>
     arma este descarcată;<br>
muniţia va fi transportată separat de armă, într-o cutie securizată;<br>
armele de foc şi muniţia vor fi transportate în cutii sau containere din fibră de sticlă, plastic, lemn sau ambalaj dur autorizat etichetate corespunzător şi încuiate astfel încât să fie protejate împotriva şocurilor şi mişcărilor bruşte.<br>
Categoriile de arme şi muniţie autorizate la transport:<br>
arme şi muniţii în scopuri sportive cum ar fi: vânătoarea, tirul cu talere, vânătoarea competiţională (toate tipurile de arme de vânătoare, inclusiv carabine pentru safari şi puşti cu lunetă);<br>
pistoale de start, arme cu bile, arme cu aer comprimat şi pusti cu bile de vopsea;<br>
arme de autoaparare (Personal Interpol, Politie de Frontiera, Ofiteri ai Serviciului de Paza si Protectie, etc.);<br>
reproduceri dupa arme şi arme antice ca de exemplu: pistoale cu pulbere, muschete, puşti vechi de peste 75 de ani.<br>
Proceduri de acceptare, condiţii de rezervare şi transport pentru pasagerii care transportă arme şi muniţie:<br>
este OBLIGATORIU ca pasagerul să declare intenţia de a transporta arme sau muniţie pe curse ;<br>
este OBLIGATORIE introducerea în PNR a menţiunii "transport arme şi/sau muniţie" (număr arme, număr containere muniţie) de către agentul emitent;<br>
cantitatea maximă de muniţie ce poate fi transportată este de 5kg, indiferent de numărul armelor deţinute de pasager;<br>
armele şi muniţia pot fi transportate numai în containere speciale, ca bagaj independent (acestea nu trebuie introduse în interiorul altui bagaj de cală);<br>
este OBLIGATORIU ca pasagerul care transportă bagaje cu arme şi/sau muniţie să se prezinte la aeroport pentru formalităţi cu minimum 1.5 ore înainte de ora prevăzută pentru decolare din biletul de călătorie.<br>
în momentul efectuării formalităţilor de check-in la aeroport, pentru acceptarea la transport a armelor şi/sau muniţiei, pasagerul are obligatia să prezinte personalului  permisul sau autorizaţia de port armă valide, iar în cazul zborurilor internaţionale are obligaţia să dovedească îndeplinirea tuturor formalităţilor legale privind ieşirea din ţară şi intrarea în ţară.<br></p>
      </div>
    </div>

    <div class="tabby-tab">
      <input type="radio" id="tab-4" name="tabby-tabs">
      <label for="tab-4">Întrebări și Răspunsuri</label>
      <div class="tabby-content">
        <img src="intrebare.jpg"/>
        <p>Cine este eligibil pentru SkyPriority?
Membrii SkyTeam Elite Plus şi clienţii cu bilete emise în clasele First şi Business.<br>
<br>
Clienţii SkyTeam Elite primesc servicii diminuate?<br>
<br>
Beneficiile nivelului Elite, aşa cum sunt prezentate pe website-urile membrilor şi SkyTeam.com rămân neschimbate şi, prin urmare, membrii Elite au dreptul la aceleaşi servicii ca înainte.<br>
<br>
Când un membru Elite cumpără un bilet în clasa First sau Business , este eligibil pentru SkyPriority?<br>
<br>
Da. Toţi pasagerii claselor First şi Business sunt eligibili pentru SkyPriority.<br>
<br>
Când un membru Elite este promovat la clasa First/Business , este eligibil pentru SkyPriority?<br>
<br>
Da. Toti pasagerii claselor First şi Business sunt eligibili pentru SkyPriority.<br>
<br>
Pasagerii pot cumpăra SkyPriority?<br>
<br>
Nu. Serviciile oferite de SkyPriority nu sunt disponibile pentru cumpărare. SkyPriority este un serviciu de călătorie special, care poate fi folosit numai de către clienţii cu bilete First şi Business şi membrii SkyTeam Elite Plus, indiferent de clasa de călătorie.<br>
Cum pot afla clienţii dacă se califică pentru SkyPriority? Vor primi un card special prin poştă?<br>
<br>
Companiile SkyTeam vor informa pasagerii eligibili în mod direct. Indicatorul SkyPriority va fi, de asemenea, imprimat pe cartea de îmbarcare.<br>
<br><br>
Ce este nevoie să facă pasagerii când ajung la aeroport?<br>
<br>
Clienţii eligibili trebuie doar să urmărească semnalistica SkyPriority :<br>
<br>
SkyPriority Check-in – cu acces într-o zonă prioritară dedicată de check-in;<br>
SkyPriority Baggage drop-off – predarea şi etichetarea bagajelor în zone dedicate;<br>
SkyPriority Security and Immigration – intrare specială pe culoarul rapid tip "Fast Track Security" şi ghişeu dedicat la controlul de frontieră. Aceste servicii sunt disponibile numai pentru anumite aeroporturi;<br>
SkyPriority Boarding – pentru îmbarcare rapidă, oferind flexibilitate prin existenţa unui culoar special la porţile de îmbarcare;<br>
Baggage 1st on belt - bagaj etichetat „Priority” pentru a fi livrat cu prioritate pe bandă la sosirea la destinaţie;<br>
Ticketing and Transfer Priority Desk – acces prioritar la serviciile oferite în aceste puncte.<br>
 

De ce au introdus SkyPriority companiile SkyTeam ?<br>
<br>
SkyTeam doreşte furnizarea celor mai bune produse şi servicii către clienţii săi.<br>
<br>
Companiile membre SkyTeam colaborează pentru îmbunatăţirea experienţei de călătorie a clienţilor – pentru a face călătoria cât mai placută pentru toţi clienţii şi în mod special pentru clienţii fideli.<br>
<br>
SkyPriority este o parte din eforturile noastre continue de îmbunătăţire a experienţei de călătorie la bordul aeronavelor  şi ale partenerilor SkyTeam. Prin îmbunătăţirea consistenţei în aeroporturi, călătoria devine mai rapidă şi mai plăcută pentru clienţii noştri, ajutându-i astfel să aleagă de fiecare dată seviciile companiilor aeriene din SkyTeam.<br>
<br>
Vă mulţumim ca aţii ales să călătoriţi la bordul aeronavelor  şi ale partenerilor SkyTeam!</p>
      </div>
    </div>
    
  </div>


    <script src="rezerva_bilete.js"></script>
    <script src="logout.js"></script>
    <script src="profil_menu.js"></script>
</body>   