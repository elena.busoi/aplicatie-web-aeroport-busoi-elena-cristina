$(document).ready(function(){


$.ajax({
    url: 'get_curse.php',
    method: 'POST',

    success: function(response){
        var data=JSON.parse(response);

       var html = '<table class="table table-striped">';
            html += '<tr>';
            var flag = 0;
            html+=`
                    <th>Id</th>
                    <th>Oras plecare</th>
                    <th>Oras destinatie</th>
                    <th>Data</th>
                    <th>Numar locuri</th>
                    <th>Durata</th>
                    <th>Pret</th>
                    <th>Ora</th>
                    <th>Șterge curse</th>`;
            html += '</tr>';
            $.each(data, function(index, value){
                html += '<tr>';
                $.each(value, function(index2, value2){
                    html += '<td>'+value2+'</td>';

                });
                html += '<td> <button style="background-color: blue; color: white;" class="sterge" data-id="'+value["id"]+'">Sterge</button></td></tr>';
            });
            html += '</table>'

            $('.f2').html(html);


            $(".sterge").click(function(e){
                e.stopPropagation();
                var id=$(this).data().id;
                var total=$(this).data().total;
                $.ajax({
                    url: 'sterge_curse.php',
                    method: 'POST',
                    data:{
                       id_cursa: id,



                    },

                    success: function(response){
                       alert(response);
                       window.location.reload(false);

                    }

                });





            })


    }


});




})




function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [month, day, year].join('/');
}

$(document).ready(()=>{
    // $('#datepicker').daterangepicker({
    //     timePicker: true,
    //     locale: {
    //       format: 'YYYY-MM-DD hh:mm:ss'
    //     }
    // });

    $('#adauga').click(function(){
        if($(this).data().type=="show"){
          $(".c2").show();
         $(this).data().type="hide";
         $('#adauga').text("Ascunde");
         $('#save').click(function(){
         $.ajax({
             url: 'adauga-curse.php',
             method: 'POST',
             data: { orasP: $('#oras_plecare').val(),
                     orasD: $('#oras_destinatie').val(),
                     data:  formatDate($('#data').val()),
                     locuri: $('#numar_locuri').val(),
                     timp : $('#timp').val(),
                     pret: $('#pret').val(),
                     ora : $('#ora').val(),


             },


             success: function(response){

                 alert(response);


             }

         });
        });
        }
        else {
         $('#adauga').text("Adauga curse");
         $(this).data().type="show";
         $(".c2").hide();

        }

   })
})
