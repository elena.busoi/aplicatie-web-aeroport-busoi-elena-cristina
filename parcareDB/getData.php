<?php
    session_start();

    include_once "../connection.php";

    $sql = "
        select * from parcare where ownerId = :ownerId
    ";

    try {
        $statement = $connection->prepare($sql);
        $statement->execute(array(
            ':ownerId' => $_SESSION['userID']
        ));
        $data = $statement->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        print_r($e);
    }
    
    echo json_encode($data);
?>