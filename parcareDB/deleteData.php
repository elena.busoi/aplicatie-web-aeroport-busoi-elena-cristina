<?php
    session_start();

    include_once "../connection.php";

    $id=$_POST['id'];

    $sql = "
        delete from parcare where id = :id
    ";

    try {
        $statement = $connection->prepare($sql);
        $statement->execute(array(
            ':id' => $id
        ));
        $data = $statement->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        print_r($e);
    }
    
    echo json_encode($data);
?>