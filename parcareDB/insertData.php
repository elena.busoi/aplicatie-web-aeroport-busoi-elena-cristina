<?php
    session_start();

    include_once "../connection.php";

    $owner=$_POST['owner'];
    $car=$_POST['car'];
    $licensePlate=$_POST['licensePlate'];
    $entryDate=$_POST['entryDate'];
    $exitDate=$_POST['exitDate'];
    $price=$_POST['price'];

    $sql = "
        insert into parcare (ownerId, owner, car, licensePlate, entryDate, exitDate, price)
        values (:ownerId, :owner, :car, :licensePlate, :entryDate, :exitDate, :price)
    ";

    $statement = $connection->prepare($sql);
    $statement->execute(array(
        ':ownerId' => $_SESSION['userID'],
        ':owner' => $owner,
        ':car' => $car,
        ':licensePlate' => $licensePlate,
        ':entryDate' => $entryDate,
        ':exitDate' => $exitDate,   
        ':price' => $price,     
    ));

    echo json_encode('Success');
?>