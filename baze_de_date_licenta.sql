-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 24, 2023 at 04:29 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `licenta`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE `administrator` (
  `ID` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `parola` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`ID`, `email`, `parola`) VALUES
(1, 'cristina@gmail.com', '095b2626c9b6bad0eb89019ea6091bd9');

-- --------------------------------------------------------

--
-- Table structure for table `curse`
--

CREATE TABLE `curse` (
  `id` int(11) NOT NULL,
  `orasP` varchar(255) NOT NULL,
  `orasD` varchar(255) NOT NULL,
  `dataP` varchar(255) NOT NULL,
  `locuri` varchar(255) NOT NULL,
  `orasD1` varchar(255) NOT NULL,
  `orasP1` varchar(255) NOT NULL,
  `dataR` varchar(255) NOT NULL,
  `locuri1` varchar(255) NOT NULL,
  `durata` varchar(255) NOT NULL,
  `pret` varchar(255) NOT NULL,
  `ora` time(6) NOT NULL,
  `companie` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `curse`
--

INSERT INTO `curse` (`id`, `orasP`, `orasD`, `dataP`, `locuri`, `orasD1`, `orasP1`, `dataR`, `locuri1`, `durata`, `pret`, `ora`, `companie`) VALUES
(1, 'Bucuresti', 'Timisoara', '06/01/2023', '86', '', '', '', '', '1 ora', '100', '13:00:00.000000', 'Wizz Air'),
(2, 'Timisoara', 'Craiova', '06/06/2023', '100', '', '', '', '', '2 ore', '70', '15:00:00.000000', 'Tarom'),
(3, 'Timisoara', 'Bucuresti', '06/02/2023', '88', '', '', '', '', '1 ore', '120', '15:00:00.000000', 'Wizz Air'),
(4, 'Cluj', 'Timisoara', '06/20/2023', '118', '', '', '', '', '1 ora', '80', '00:00:00.000000', 'Wizz Air'),
(5, 'Timisoara', 'Roma', '08/12/2023', '200', '', '', '', '', '3 ore', '275', '02:00:00.000000', 'Tarom'),
(6, 'Bucuresti', 'Londra', '09/19/2023', '199', '', '', '', '', '3 ore', '425', '10:00:00.000000', 'Wizz Air'),
(7, 'Bucuresti', 'Paris', '06/20/2023', '200', '', '', '', '', '4 ore', '315', '07:00:00.000000', 'Tarom');

-- --------------------------------------------------------

--
-- Table structure for table `parcare`
--

CREATE TABLE `parcare` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `owner` varchar(255) NOT NULL,
  `car` varchar(255) NOT NULL,
  `licensePlate` varchar(255) NOT NULL,
  `entryDate` varchar(255) NOT NULL,
  `exitDate` varchar(255) NOT NULL,
  `price` int(11) DEFAULT 10
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `parcare`
--

INSERT INTO `parcare` (`id`, `ownerId`, `owner`, `car`, `licensePlate`, `entryDate`, `exitDate`, `price`) VALUES
(4, 2, 'Sisu Cristian', 'Skoda', 'Gj73SOC', '2023-05-15', '2023-05-18', 0),
(5, 10, 'Sisu', 'Skoda', 'Gj73SOC', '2023-06-27', '2023-06-30', 0),
(6, 10, 'Sisu', 'Skoda2', 'Gj71SOC', '2023-06-26', '2023-06-28', 0),
(7, 10, 'Sisu', 'Skoda', 'Gj73SOC', '2023-06-07', '2023-06-15', 0),
(8, 10, 'Sisu', 'Skoda', 'Gj71SOC', '2023-06-29', '2023-06-30', 0),
(37, 11, 'Sisu2', 'Skoda2', 'Gj72SOC', '2023-06-16', '2023-06-23', 0),
(49, 4, 'Bianca Busoi', 'Skoda', 'GJ72SOC', '2023-06-07', '2023-06-17', 100);

-- --------------------------------------------------------

--
-- Table structure for table `rezervari`
--

CREATE TABLE `rezervari` (
  `id` int(11) NOT NULL,
  `id_user` varchar(255) NOT NULL,
  `id_curse` varchar(255) NOT NULL,
  `locuri` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `pret` int(11) NOT NULL,
  `plata` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `rezervari`
--

INSERT INTO `rezervari` (`id`, `id_user`, `id_curse`, `locuri`, `status`, `pret`, `plata`) VALUES
(26, '4', '1', '1', 'Aprobat', 100, 1),
(27, '4', '1', '1', 'In asteptare', 175, 0),
(31, '4', '6', '1', 'Aprobat', 425, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `Fname` varchar(255) NOT NULL,
  `Lname` varchar(255) NOT NULL,
  `Phone` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Adresa` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `Fname`, `Lname`, `Phone`, `Email`, `Password`, `Adresa`) VALUES
(1, 'Andrei', 'Munteanu', '0771223480', 'andrei.munteanu1398@gmail.com', '095b2626c9b6bad0eb89019ea6091bd9', 'Timisoara'),
(2, 'Cristian', 'Sisu', '0768956442', 'sisuocristian@gmail.com', '095b2626c9b6bad0eb89019ea6091bd9', 'Craiova'),
(3, 'Alex', 'Drula', '0789654892', 'alex.drula@gmail.com', '5c90b96a75d4f9d5a1cfaa6f532afdc8', 'TG-Jiu'),
(4, 'Bianca', 'Busoi', '0765835009', 'bianca@yahoo.com', '095b2626c9b6bad0eb89019ea6091bd9', 'Cluj'),
(5, 'Daniela', 'Nicola', '0745698787', 'daniela@gmail.com', '095b2626c9b6bad0eb89019ea6091bd9', 'Bacau'),
(6, 'Adrian', 'Dumitru', '0745694598', 'adrian@gmail.com', '095b2626c9b6bad0eb89019ea6091bd9', 'Oradea'),
(7, 'Octavian', 'Bobina', '0772014598', 'octavian@gmail.com', '095b2626c9b6bad0eb89019ea6091bd9', 'Arad'),
(8, 'Cristina', 'Busoi', '0769313039', 'cristina23051@yahoo.com', '5c90b96a75d4f9d5a1cfaa6f532afdc8', ''),
(9, 'Razvan', 'Silviu', '0789654236', 'razvan.silviu@gmail.com', '095b2626c9b6bad0eb89019ea6091bd9', ''),
(10, 'Izabela', 'Cretan', '0789654235', 'izabela.cretan@gmail.com', '095b2626c9b6bad0eb89019ea6091bd9', ''),
(11, 'Denisa', 'Calota', '0789654258', 'denis.calota@gmail.com', '095b2626c9b6bad0eb89019ea6091bd9', ''),
(12, 'ion', 'marin', '0789658954', 'ion.marin@gmail.com', '095b2626c9b6bad0eb89019ea6091bd9', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `curse`
--
ALTER TABLE `curse`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parcare`
--
ALTER TABLE `parcare`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rezervari`
--
ALTER TABLE `rezervari`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrator`
--
ALTER TABLE `administrator`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `curse`
--
ALTER TABLE `curse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `parcare`
--
ALTER TABLE `parcare`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `rezervari`
--
ALTER TABLE `rezervari`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
