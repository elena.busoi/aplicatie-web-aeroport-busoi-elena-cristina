<?php
    session_start();

    $errorMsg = '';
    if (isset($_SESSION['error_login'])) {
        $errorMsg = $_SESSION['error_login'];

        session_destroy();
    }
?>

<!DOCTYPE html>
<html>
<head>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<link rel="stylesheet" type="text/css" href="style.css">

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>
<body>
<div class="lg" id="lg-form" align="center" >
    <div class="lg-form">
        <p id="msg" class="msg"></p>
        <p class="sign" align="center">Conectare</p>
        <div>
            <?php echo $errorMsg; ?>
        </div>
        <form class="form1" action="login.php" method="POST">
            <input type="text" class="input" id="email" name="sendEmail" placeholder="Email"><br>
            <input type="password" class="input" id="password" name="sendPassword" placeholder="Parolă">
            <button type="submit" class="btn btn-primary  log-in-button" id="submit">Conectează-te</button><br>
            <button type="button" class="btn btn-secondary register-button" id="register">Înregistrează-te</button><br>
            <!-- <h1><img  height="120"class="logo logo2" src="avion.png"></h1> -->

        </form>
    </div>
</div>
<div class="cc" id="register-form" style="display:none" align="center">
    <div class="create-account-form">
        <p class="sign" align="center">Cont nou</p>
        <form class="form1">
            <input type="text" class="input" id="first-name" name="first-name" placeholder="Prenume"><br>
            <input type="text" class="input" id="last-name" name="last-name" placeholder="Nume"><br>
            <input type="text" class="input" id="phone" name="phone" placeholder="Număr de telefon"><br>
            <input type="text" class="input" id="email2" name="email2" placeholder="Email">
            <input type="password" class="input" id="password2" name="password2" placeholder="Parolă">
            <button type="button" class="btn btn-outline-secondary create-account-button" id="create-account">Creează cont</button>
            <button type="button" class="btn btn-link linkbtn" id="linkbtn">înapoi la pagina de conectare</button>


        </form>
    </div>
</div>
<script src="register.js"></script>
<script src="Create_account.js"></script>
<script src="linkbtn.js"></script>
</body>
</html>
